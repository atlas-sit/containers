#!/bin/sh
#==================================================================
# This script sets runtime environment for AthSimulation containers 
# Author: Alex Undrus
#==================================================================
#
# The variables below has to be adjusted for each container
#
# AtlasBuildBranch is the git branch the release is built against
export AtlasBuildBranch=21.0
# AtlasVersion is the release name
export AtlasVersion=21.0.109
# AtlasProject is the project name
export AtlasProject=AthSimulation
# INSTALLATION_ROOT is the release installation root directory
INSTALLATION_ROOT=/cvmfs/atlas.cern.ch/repo
# BINARY_TAG is the release platform
export BINARY_TAG=x86_64-slc6-gcc62-opt
#ATLAS_DB_AREA and ATLAS_POOLCOND_PATH point to ReleaseDB and conditions db areas
export ATLAS_DB_AREA=/cvmfs/atlas.cern.ch/repo/sw/database
export ATLAS_POOLCOND_PATH=/cvmfs/atlas-condb.cern.ch/repo/conditions
#==============
SITEROOT=${INSTALLATION_ROOT}/sw/software/${AtlasBuildBranch}
#Historically ATLAS_RELEASE_BASE and AtlasBaseDir were used in the past
#For modern releases they are not requires
#export ATLAS_RELEASE_BASE=${AtlasBaseDir}
#export AtlasBaseDir=${SITEROOT}
export AtlasArea=${SITEROOT}/${AtlasProject}/${AtlasVersion}
export G4PATH=${SITEROOT}/Geant4
export LCG_RELEASE_BASE=${SITEROOT}/sw/lcg/releases
export LCG_PLATFORM=${BINARY_TAG}
export PATH=/opt/lcg/binutils/2.28/x86_64-slc6/bin:\
${INSTALLATION_ROOT}/ATLASLocalRootBase/x86_64/Cmake/current/Linux-x86_64/bin:\
${LCG_RELEASE_BASE}/gcc/6.2.0binutils/x86_64-slc6/bin:\
${PATH}
[[ "${LANG}" = "" ]] && export LANG=C
[[ "${LC_ALL}" = "" ]] && export LC_ALL=C
gcc_area=`which gcc`
if [[ "${gcc_area}" != "" ]]; then
dir_gcc1=`dirname ${gcc_area}`
dir_gcc=`dirname ${dir_gcc1}`
export CXX=${dir_gcc1}/g++
export FC=${dir_gcc1}/gfortran
export CC=${dir_gcc1}/gcc
export COMPILER_PATH=${dir_gcc}/lib64
fi
source ${SITEROOT}/${AtlasProject}/${AtlasVersion}/InstallArea/${BINARY_TAG}/setup.sh
export LD_LIBRARY_PATH=/opt/lcg/binutils/2.28/x86_64-slc6/lib:\
${LCG_RELEASE_BASE}/gcc/6.2.0binutils/x86_64-slc6/lib64:\
${LD_LIBRARY_PATH}

# unsetting FRONTIER_SERVER is required in containers
unset FRONTIER_SERVER

#echo ==============================
#echo =======FEW POSSIBLE TESTS
#echo ==============================
#athena AthenaPython/test_pyathena.py
#athena AthExHelloWorld/HelloWorldOptions.py
